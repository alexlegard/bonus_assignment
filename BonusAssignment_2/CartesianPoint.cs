﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BonusAssignment_2
{
    public class CartesianPoint
    {
        /*Attributes:
         * 
         * X-coord
         * Y-coord
         * 
         * Methods:
         * 
         * getQuadrant*/

        public int xcoord;
        public int ycoord;

        //CONSTRUCTOR
        public CartesianPoint(int x, int y)
        {
            xcoord = x;
            ycoord = y;
        }

        public int quadrant(int x, int y)
        {
            if ((x == 0) | (y == 0))
            {
                return 0; // Return 0 if input is invalid
            }
            else if ((x > 0) & (y > 0))
            {
                return 1; //Quadrant 1
            }
            else if ((x < 0) & (y > 0))
            {
                return 2;//Quadrant 2
            }
            else if ((x < 0) & (y < 0))
            {
                return 3;//Quadrant 3
            }
            else if ((x > 0) & (y < 0))
            {
                return 4;//Quadrant 4
            }
            return 0;
        }

        public String info(int x, int y)
        {
            return x + ", " + y;
        }
    }/*Class end*/
}
