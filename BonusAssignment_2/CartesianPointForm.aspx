﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CartesianPointForm.aspx.cs" Inherits="BonusAssignment_2.CartesianPointForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bonus Assignment 1</title>
</head>
<body>
    <form id="form1" runat="server">
        <div> <!--We want a div to wrap around everything in the form-->
            <h1>Bonus Assignment 1</h1>
            <div><!--We also want a div for every input element or button-->
                <p>X coordinate: </p><asp:TextBox runat="server" ID="pointXCoordinate" placeholder="X position">
                </asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an integer."
                    ControlToValidate="pointXCoordinate" ID="RequiredXValidator">
                </asp:RequiredFieldValidator>
            </div>
            <div>
                <p>Y position: </p><asp:TextBox runat="server" ID="pointYCoordinate" placeholder="Y position">
                </asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an integer."
                    ControlToValidate="pointYCoordinate" ID="RequiredYValidator">
                </asp:RequiredFieldValidator>
            </div>
            <div>
                <p>Prime number test: </p><asp:TextBox runat="server" ID="primeTestNumber" placeholder="Enter a number">
                </asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a test number."
                    ControlToValidate="primeTestNumber" ID="PrimeTestValidator">
                </asp:RequiredFieldValidator>
            </div>
            <div>
                <p>Palindrome test: </p><asp:TextBox runat="server" ID="palTestWord" placeholder="Enter a word">
                </asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a test word."
                    ControlToValidate="palTestWord" ID="PalTestValidator">
                </asp:RequiredFieldValidator>
            </div>
            </br>
            <div><!--Button-->
                <asp:Button runat="server" ID="cartesianFormButton" text="Submit" OnClick="buttonClick" />
            </div>
        </div>

        <!--Codebehind will inject the solution for the first question: Which quadrant does the cartesian point fall under?-->
        <div runat="server" id="cartesianSolution">
            <p>Cartesian solution</p>
        </div>

        <!--Solution for the second question-->
        <div runat="server" id="primeSolution">
            <p>Prime solution</p>
        </div>

        <!--Solution for the third question-->
        <div runat="server" id="palindromeSolution">
            <p>Palindrome solution</p>
        </div>
    </form>
</body>
</html>
