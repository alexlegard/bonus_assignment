﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_2
{
    public partial class CartesianPointForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonClick(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            //Fetch information from the web form
            int xcoord = int.Parse(pointXCoordinate.Text);
            int ycoord = int.Parse(pointYCoordinate.Text);
            int testPrime = int.Parse(primeTestNumber.Text);
            String testPal = palTestWord.Text.ToString();

            //Create the objects
            CartesianPoint point = new CartesianPoint(xcoord, ycoord);
            Number number = new Number(testPrime);
            Word word = new Word(testPal);

            //Use the respective class methods and output to the console
            int quadrant = point.quadrant(xcoord, ycoord);
            bool isPrime = number.isPrime();
            bool isPal = word.isPalindrome();
            String cartKey = "Point is in quadrant: " + quadrant.ToString();
            String primeKey = "Test number is prime: " + isPrime;
            String palKey = "Word is a palindrome: " + isPal;
            cartesianSolution.InnerHtml = cartKey;
            primeSolution.InnerHtml = primeKey;
            palindromeSolution.InnerHtml = palKey;

        }/*End buttonClick method*/
    }
}