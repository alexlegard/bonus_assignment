﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BonusAssignment_2
{
    public class Number
    {
        /* Attributes: Value
         * 
         * Methods: isPrime (boolean)
         */
        public int value;

        //Constructor
        public Number(int v)
        {
            value = v;
        }

        //Methods
        public bool isPrime()
        {
            if(value == 0)
            {
                return false; // Because 0 isn't prime?
            } else if(value == 1)
            {
                return true;
            } else if(value == 2)
            {
                return true;
            }
            bool flag = true;

            for(int c = value-1; c > 1; c--)
            {
                System.Diagnostics.Debug.WriteLine("Increment: " + c);
                if ((value % c) == 0)
                {
                    flag = false;
                    System.Diagnostics.Debug.WriteLine("Flag false");
                }
            }
            return flag;
        }

        public int get()
        {
            return value;
        }
    }
}