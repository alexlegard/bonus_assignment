﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace BonusAssignment_2
{
    public class Word
    {
        /*Add attributes
         * seq
         */
        public String str;

        /*Add constructor*/
        public Word(String s)
        {
            str = s;
        }

        /*Add methods*/
        public bool isPalindrome()
        {
            String lowerStr = str.ToLower();
            String strNoSpace = lowerStr.Replace(" ", string.Empty);
            System.Diagnostics.Debug.WriteLine("strNoSpaces: " + strNoSpace);

            /*We're using a StringBuilder to make the reversed string. Because Strings are immutable, if we used regular
             * strings to make the reverse string we'd have to create a new String every time we add a letter.*/
            StringBuilder builder = new StringBuilder();

            /*Decrement from string length down to 1.*/
            for(int i = strNoSpace.Length; i > 0; i--)
            {
                System.Diagnostics.Debug.WriteLine("Decrement: " + i);
                builder.Append(strNoSpace[i-1]);
                System.Diagnostics.Debug.WriteLine("String: " + builder.ToString());
            }
            String backwardStr = builder.ToString();
            if(strNoSpace == backwardStr)
            {
                return true;
            }
            return false;
        }
    }
}