﻿/*Not using this class in my project, but I'm putting it here because I don't want to keep switching classes to look at my
 * code.*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mytest
{
    public class Client
    {
        public String name;
        public int age;
        public String phone;
        public String email;
        public List<String> medicalConditions;
        public String experience;

        //ADD A CONSTRUCTOR
        public Client(String clientName, int clientAge, String clientPhone, String clientEmail,
            List<String> clientMedicalConditions, String clientExperience)
        {
            name = clientName;
            age = clientAge;
            phone = clientPhone;
            email = clientEmail;
            medicalConditions = clientMedicalConditions;
            experience = clientExperience;
        }

        public String info()
        {
            Console.WriteLine("Name:" + name);
            Console.WriteLine("Age:" + age);
            Console.WriteLine("Phone:" + phone);
            Console.WriteLine("Email:" + email);
            Console.WriteLine("Medical conditions:" + medicalConditions);
            Console.WriteLine("Experience:" + experience);

            String output = "Name: " + name;
            output += "Age: " + age;
            output += "Phone number: " + phone;
            output += "Email: " + email;
            output += "Medical conditions: " + medicalConditions;
            output += "Experience: " + experience;

            return output;
        }
    }
}